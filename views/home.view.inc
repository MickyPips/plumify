<div id="track-selection" class="clearfix">
	<header class="row">
		<span class="col-xs-6">
			<h2>Your Library</h2>
		</span>
		<span class="col-xs-6 text-right">
			<a href="#" class="btn btn-success">Discover Artists</a>
		</span>
	</header>
	<?php require_once(COMPONENTS_DIR . "track-library.php"); ?>
</div>