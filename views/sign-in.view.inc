<div id="form-cont">
	<div id="signin-form" class="text-center">
		
		<h1>Sign in<h1>
		<hr />

		<form id="loginForm">
			<div class="input-group">
				<label class="sr-only" for="email">Email</label>
				<span class="input-group-addon"><span class="fa fa-user"></span></span>
				<input class="form-control" type="email" id="email" name="email" placeholder="Email" required />	
			</div>
			<div class="input-group">
				<label class="sr-only" for="password">Password</label>
				<span class="input-group-addon"><span class="fa fa-lock"></span></span>
				<input class="form-control" type="password" id="password" name="password" placeholder="Password" required />
			</div>
			<button id="user-signin" class="btn btn-success" type="submit" name="submit" value="Login" title="Login" data-set="user">Login</button>
		</form>								
	</div>
</div>