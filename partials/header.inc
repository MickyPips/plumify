<?php
  /**
   * Serves browser with correct views based on URI.
   */

  /**
   * Start session.
   */
  session_start();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
  <?php
    
    /*
     * Require stylesheets and javascript files.
     */
    require_once('header-scripts.inc');

    /**
     * Require the global controller file.
     */
    require_once(CONTROLLERS_DIR . 'global.controller.php');

    /**
     * Require the global variable file.
     */
    require_once(VARS_DIR . 'global.vars.php');

   /**
    * Try/Catch $_SESSION cookie checks and redirects to appropriate view.
    * @throws Exception if can't check cookies or redirect
    */
    try {

      if(!isset($_SESSION['auth'])) {

        reDirectToLogin($signInURL);

      } else {
        
        if(!isset($_SESSION['user']) ) {
          
          reDirectToLogin($signInURL);

        } else {

          $currentAuth = md5( USER_AGENT . USER_IP . LOGIN_TIME);
          $authSession = $_SESSION['auth'];

          if($authSession === $currentAuth) {

            require_once(VARS_DIR . 'user.vars.php');

          } else {
            reDirectToLogin($signInURL);
          }
        }
      }
    } catch (Exception $e) {
      echo $e->getMessage();
    }

    /**
     * Require the view controller file.
     */
    $currentController ? require_once($currentController) : "";

  ?>

  <title><?php echo $metaTitle; ?></title>
  <meta name="robots" content="noindex,nofollow">
</head>
<body class="<?php echo $currentView; ?>">
<header id="site-header">
  <div class="site-info container-fluid">
    <div class="col-md-6 pull-left">
      <div class="header-logo">
        <a href="/" title="Plumify" id="site-logo"></a>
      </div>
    </div>

      <?php

      if(!$isSignIn):

        require_once(COMPONENTS_DIR . 'main-menu.php');

      endif; ?>

  </div>

</header>
<main role="main" id="main-content" class="container-fluid">
  <section id="page-content" class="row">

    <?php if(!$isHome && !$isSignIn): ?>
      <h1>
        <?php echo $viewTitle; ?>
      </h1>
      <hr />
    
    <?php endif;

      if($isSignIn):

    ?>

      <section class="view col-md-12">

    <?php

      else:

    ?>

      <section class="view col-md-8">
   
    <?php

      endif;

      /**
       * Check if there is any content for the current view, if there is then require it
       */
      $viewExists ? require_once($viewContent) : require_once(ERROR_DIR . '404.inc');

    ?>

    </section>
    <aside class="col-md-3 col-md-offset-1 sidebar-cont">
      <?php

        if(!$isSignIn):

          require_once(PARTIALS_DIR . "sidebar.inc");

        endif;

      ?>
    </aside>
  </section>
</main>