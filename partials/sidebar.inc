<div class="sidebar">
	<h2>Activity</h2>
	<ul>

	<?php 
		
		$users = "";

		foreach ($allUsers as $user) {

			if($user['name'] != $_SESSION['user']['name']) {

				$name = $user['name'];
				$listenedTo = $user['recent_activity']['last_listened'];
				$avatar = $user['avatar'];

				$users .= <<<OUT

					<li>
						<a href="#" class="$avatar" title="View $name's profile"></a>
						<div class="activity-info">
							<p><small><b>$name</b></small></p>
							<p><x-small>listened to <b><a class="blue" href="#" title="Listen to $listenedTo">$listenedTo</a></b></x-small></p>
						</div>
					</li>
OUT;

			}

		}

		echo $users;

	?>

	</ul>
	<nav class="text-center" role="navigation">

		<button class="btn btn-default" title="Previous" disabled="disabled">Prev</button>
		<button class="btn btn-default" title="Next">Next</button>

	</nav>
</div>