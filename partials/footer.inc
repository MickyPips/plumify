  
    <footer id="footer">
      <?php

        if(!$isSignIn) {
            require_once(COMPONENTS_DIR . "audio-player.php");
        }

        echo ajaxRes(array('hideTrigger' => true));
        require_once('footer-scripts.inc');
      ?>
    </footer>
    
  </body>
</html>