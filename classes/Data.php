<?php
	/**
	 * Data class.
	 */

	class Data {

		/**
		 * Defines name of data file.
		 *
		 * @var string
		 */
		protected $filename;

		/**
		 * Defines url for data file.
		 *
		 * @var string
		 */
		protected $fileUrl;

		/**
		 * Constructor function for datasets.
		 * @param string $filename
		 */
		function __construct($filename)
		{
			$this->setDataFilename($filename);
		}

		/**
		 * Setter method for the data filename.
		 *
		 * @param string $dataFile String value for data file.
		 * @return string $err_msg
		 */
		public function setDataFilename($dataFile)
		{

			$this->filename = $dataFile . ".data.json";

		}

		/**
		 * Setter method for the data file url.
		 *
		 * @param string $fileUrl String value for data file url.
		 */
		protected function setDataUrl($file)
		{

			return __DIR__ . "/../data/" . $file;

		}

		/**
		 * Getter method for the data filename.
		 *
		 * @return string Filename value for the data file.
		 */
		public function getDataFilename()
		{
			return $this->filename;
		}
		
		/**
		 * Read data.
		 *
		 * @param array $dataArr Data array containing paramaters to be used to get data to read.
		 *
		 * @return array Decoded JSON data from specified file
		 * @return string $error_msg
		 */
		public function getData($dataArr = array())
		{			

			try {
				
				$dataFile = $this->getDataFilename();
				$key = explode('.', $dataFile);
				
				$dataUrl = $this->setDataUrl($dataFile);
				
				$jsonData = file_get_contents($dataUrl);

				$dataArray = json_decode($jsonData, true);

				if($dataFile != '') {

					if(!$jsonData) {
						
						$err_msg = '<b>' . $dataUrl . '</b> - File does not exist or cannot be opened';

						return $err_msg;
					}

					return $dataArray;				
				}

			} catch (Throwable $t) {
				return $t;
			}
		}

		/**
		 * Get the latest update.
		 * @return array
		 */
		public function latestUpdate() {

			$data = $this->getData();
			$user = array();
			$recent = 0;

			foreach ($data as $value) {				

				if(array_key_exists('recent_activity', $value)) {

					if($value['recent_activity']['share']['time'] > $recent) {

						$recent = $value['recent_activity']['share']['time'];
						$user = $value;
					} 
				}
			}
			return $user;
		}
	}

?>