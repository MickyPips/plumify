<?php
	
	namespace Site;

	/**
	 * Site info class.
	 *
	 * Class that sets the information about the portal.
	 */
	class Info
	{
		/**
		 * Defines id of head office.
		 *
		 * @var number
		 */
		protected static $headOfficeID;

		/**
		 * Defines id of site.
		 *
		 * @var number
		 */
		protected static $siteID;

		/**
		 * Defines title of site.
		 *
		 * @var string
		 */
		protected static $title;
		
		/**
		 * Setter method for the site id property.
		 *
		 * @param number $t Numeric value for site id.
		 */
		static function setHeadOfficeID($hid)
		{
			self::$headOfficeID = $hid;
		}

		/**
		 * Setter method for the site id property.
		 *
		 * @param number $t Numeric value for site id.
		 */
		static function setSiteID($sid)
		{
			self::$siteID = $sid;
		}

		/**
		 * Setter method for the site title property.
		 *
		 * @param string $t String value for new site title.
		 */
		static function setSiteTitle($t)
		{
			self::$title = $t;
		}

		/**
		 * Getter method for the head office id property.
		 *
		 * @return number ID of the head office.
		 */
		static function getHeadOfficeID()
		{
			return self::$headOfficeID;
		}

		/**
		 * Getter method for the site id property.
		 *
		 * @return number ID of the site.
		 */
		static function getSiteID()
		{
			return self::$siteID;
		}

		/**
		 * Getter method for the site title property.
		 *
		 * @return string Title of the site.
		 */
		static function getSiteTitle()
		{
			return self::$title;
		}
	}

	/**
	 * Site content class.
	 *
	 * Class to set the content for the portal
	 */
	class Content
	{

		/**
		 * Function to check if current view is the sign in page.
		 *
		 * @return boolean $signIn Returns true / false.
		 */
		static function isSignIn()
		{

			$_SERVER['REQUEST_URI'] === '/sign-in' ? $signIn = true : $signIn = false;

			return $signIn;
		}
		/**
		 * Function to check if current view is the home page.
		 *
		 * @return boolean $home Returns true / false.
		 */
		static function isHome()
		{

			$_SERVER['REQUEST_URI'] === '/' ? $home = true : $home = false;

			return $home;
		}
		/**
		  * Function to get the current URI, modify it if it is the index page, then return it.
		  *
		  * @return string Returns current URI.
		  */
		static function getCurrentView()
		{

			$view = basename($_SERVER["REQUEST_URI"]);

			$view == '' ? $view = 'home' : '';

			$view = preg_replace('/\?.*/', '', $view);  

			return $view;
		}

		/**
		  * Function to get the URL to current view file.
		  *
		  * @return string Returns URL to current view file.
		  */

		static function currentViewFile()
		{

			$view = self::getCurrentView();

			return VIEWS_DIR . $view . ".view.inc";
		}

		/**
		 * Function to retuen the title of the current view.
		 *
		 * @return string Returns a trimmed, human readable, formatted version of the URI.
		 */
		static function viewTitle()
		{

			$seperate = explode("-", self::getCurrentView());

			$viewTitle  = "";

			foreach ($seperate as $component) {

			  $component = trim($component);

			  $viewTitle .= $component . " ";
			}

			$viewTitle = trim(ucwords($viewTitle));

			return $viewTitle;
		}
		/**
		 * Function that checks for view specific controllers and requires them.
		 */
		static function getController()
		{

			switch ($_SERVER['REQUEST_URI']) {
			  case '/':
			    $controller = "index";
			    break;
			  default:
			    $controller = self::getCurrentView();
			};    


			$pageController = VIEW_CONTROLLERS_DIR . $controller . ".controller.php";

			if(file_exists($pageController)) {

			  return $pageController;
			
			} else {

				return false;
			}
		}

		/**
		 * Renders both sidebar or dashboard menus.
		 *
		 * Renders both sidebar or dashboard menus depending on if the $sidebar argument is passed
		 * @param array $items Array contianing the menu item objects
		 * @param boolean $sidebar Default set to false
		 * @return 
		 */
		static function getMenu($items, $sidebar = false)
		{

			if(count($items) > 0) {

				$menu = $sidebar ? "<aside id='aside' class='sidebar col-md-2'>" : "<section id='dashboard-menu' class='col-sm-12 clearfix'>";
				$menu .= $sidebar ? "<ul><li class='menu-item clearfix'><a href='/' title='Dashboard'" . (VIEW_BASENAME === "" ? "class='active'" : "") . ">Dashboard</a></li>" : "";

				
				foreach ($items as $menuItem => $prop) {
					
					$title = ucfirst($prop->getTitle());	
					$link = str_replace(array(' ', '*'), '-', strtolower($prop->getName()));
					
					$menu .= $sidebar ? $prop->renderSidebarItem($link, $title) : $prop->renderMenuItem($link, $title);

				}

				$menu .= $sidebar ? "</ul>" : "";
				$menu .= $sidebar ? "</aside>" : "</section>";

				return $menu;
			}
		}
	}

?>