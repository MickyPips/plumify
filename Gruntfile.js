module.exports = function(grunt) {

	require("load-grunt-tasks")(grunt);

	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),
		
		//modernizr
		modernizr: {
			cache: true,
			dist: {
				dest: 'assets/libs/modernizr.min.js'
			},
			uglify: true
		},
		//clean files
		clean: {
			css: ['assets/css/*.css', 'assets/css/*.map'],
			js: ['assets/js/*.js']
		},
		//sprites
		sprite:{
      all: {
        src: ['dev/sprites/*.png', 'assets/imgs/thumbs/*.png'],
        retinaSrcFilter: ['dev/sprites/*@2x.png', 'assets/imgs/thumbs/*@2x.png'],
        dest: 'assets/imgs/spritesheet.png',
				retinaDest: 'assets/imgs/spritesheet.retina@2x.png',
        destCss: 'dev/scss/vars/_sprites.scss'
      }
    },
    //concat
    concat: {
      dist: {
        src: ['dev/js/*.js'],
        dest: 'dev/tmp/concat.js'
      }
    },
		//babel
		babel: {
		  options: {
		    sourceMap: true
		  },
		  dist: {
		    files: {
		      "dev/tmp/main.js": "dev/tmp/concat.js"
		    }
		  }
		},
		//minify js
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
			},
			build: {
				src: 'dev/tmp/main.js',
				dest: 'assets/js/main.min.js'
			}
		},
		//convert scss to css
		sass: {                              
		    dist: {                            
		      options: {                       
		        style: 'expanded'
		      },
		      files: {                        
		        'dev/tmp/main.css': 'dev/scss/main.scss'
		      }
		    }
		},
		//prefix
		postcss: {
            options: {
                map: true,
                processors: [
                    require('autoprefixer')
                ]
            },
            dist: {
                src: 'dev/tmp/main.css'
            }
        },
		//minify css
		cssmin: {
			target: {
				files: [{
					expand: true,
					cwd : 'dev/tmp',
					src: ['*.css', '!*.min.css'],
					dest: 'assets/css',
					ext: '.min.css'
				}]
			}
		},
		//watch sass and js files
		watch: {
			sass: {
				files: ['dev/scss/**/*.scss'],
				tasks: ['compileSCSS']
			},
			js: {
				files: ['dev/js/*.js'],
				tasks: ['compileJS']
			},
			sprites: {
				files: ['dev/sprites/*.png'],
				tasks: ['compileSprites']
			}
		}

	});

	//watch
	grunt.loadNpmTasks('grunt-contrib-watch');
	//clean
	grunt.loadNpmTasks('grunt-contrib-clean');
	//modernizr
	grunt.loadNpmTasks("grunt-modernizr");
	//spritesmith
	grunt.loadNpmTasks('grunt-spritesmith');
	//sass
	grunt.loadNpmTasks('grunt-contrib-sass');
	//min.js
	grunt.loadNpmTasks('grunt-contrib-uglify');
	//prefix
	grunt.loadNpmTasks('grunt-postcss');
	//min.css
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	//concat js
  grunt.loadNpmTasks('grunt-contrib-concat');

	//Default tasks
	grunt.registerTask('default', ['clean', 'modernizr', 'sprite', 'concat',  'babel', 'uglify', 'sass', 'postcss', 'cssmin']);

	//Generate sprites
	grunt.registerTask('compileSprites', ['sprite']);

	//Recomplie SASS
	grunt.registerTask('compileSCSS', ['clean:css', 'sass', 'postcss', 'cssmin']);

	//Recomplie js
	grunt.registerTask('compileJS', ['clean:js', 'concat', 'babel', 'uglify']);	

};