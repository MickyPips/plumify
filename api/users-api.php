<?php
	/**
	 * User API Controller.
	 */	

	/**
	 * Require authentication file.
	 */
	require_once('api-check.php');

	if(vaildReq($_POST)) {
		
		/**
		 * Require the main user controller file.
		 */
		include_once(__DIR__ . '/../controllers/user.controller.php');

		/**
		 * Alter response depending on type of request was sent.
		 */
		switch($_POST['req']) {

			case 'usr_login':
				
				try {

					$userLogin = userLogin($_POST['dataset'], $_POST['email'], $_POST['password']);

					if(!$userLogin) {
						throw new Exception();
					}

				} catch (Exception $e) {
					echo $e->getMessage();
				}
				break;
			}
	}

?>