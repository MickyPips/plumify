<?php
	/**
	 * Authentication checker for API calls.
	 */

	/**
	 * Access $_SESSION superglobal.
	 */
	session_start();

	/**
	 * Include the config file
	 */
	include_once(__DIR__ . "/../config.php");

	function vaildReq($data) {

		$authenticated = false;

		$steps = "";

		if(isset($data['req']) && !empty($data['req']) && $data['req'] != 'usr_login') {

			/**
			 * Check and set authentication state.
			 */
			if(!$_SESSION['auth'] && !$_SESSION['user']) {

				$authenticated = false;

			} else {

				$currentAuth = md5( USER_AGENT . USER_IP . LOGIN_TIME);
				$authSession = $_SESSION['auth'];

				$authenticated = ($authSession === $currentAuth);

			}

			$steps .= "Authenticated: $authenticated (auth: " . $_SESSION['auth'] . ", usr: " . $_SESSION['user'] . ")";
		}

		if(isset($data['req']) && !empty($data['req']) && $authenticated || $data['req'] === 'usr_login'):

			/**
			 * Define required fields in an array.
			 */
			$required = array('req');

			/**
			 * Alter required fields array depending on type of request.
			 */
			switch($data['req']) {

				case 'usr_login':
					array_push($required, 'email', 'password');
					break;				
			}

			/**
			 * Empty array to push any missing fields, for user feedback.
			 */
			$formVaildate = array();

			/**
			 * Default value for request, assume everything is OK.
			 */
			$vaildReq = true;	

			/**
			 * Loop over each required field name and check that it is set in $data,
			 * if it isn't then add it to the missing fields array and set the validation to false;
			 * if it is set then generate a variable based on the value.
			 */
			foreach ($required as $value) {

				if(!isset($data[$value]) || empty($data[$value])) {

					array_push($formVaildate, $value);

					$vaildReq = false;
				
				} else {

					${$value} = $data[$value];

				}
			}
			
			/**
			 * Check if headers contain AJAX data and that the required fields are present.
			 */
			if ($vaildReq === false) {

				/**
				 * If header data is OK but there are missing fields then
				 * return error header, json encoded array of missing fields then die.
				 */
				$missingFields = "";

				/**
				 * Create a comma seperated string from values in the missing fields array.
				 */
				foreach ($formVaildate as $key => $field) {					
					$missingFields .= $field . (($key + 1) < count($formVaildate) ? ', ' : '');
				}

				$res = array("res" => "error", "msg" => "Missing Fields: $missingFields.");
				echo json_encode($res);
				die();

			
			} else {

				return $vaildReq;

			}

		else:
		
			/**
			 * If headers don't contain the correct data return error header and message then die.
			 */
			$res = array("res" => "error", "msg" => "Invalid request $steps " . $data['req']);
			echo json_encode($res);
			die();
		
		endif;
	}

?>