<?php
  /**
   * Stores all constants and configuration variables.
   *
   */

  /**
   * Variable that controls the visiblity of PHP errors, should be set to false in production.
   *
   * @var boolean
   */
  $show_errors = true;  

  if($show_errors) {
    ini_set("display_errors", "1");
    error_reporting(E_ALL);
  } else {
    error_reporting(0);
  }

  /**
   * Microsecond timestamp of user login.
   * @var timestamp Unix microsecond timestamp.
   */
  $getTime = setTime();
  
  /**
   * Get login timestamp with microseconds.
   * @return timestamp Unix timestamp with microseconds.
   */
  function setTime()
  {

    if(defined('LOGIN_TIME')) {

      return microtime();
    }
  }

  /**
   * Get visitors' IP address.
   *
   * @return string IP address of current visitor.
   */
  function getUserIP()
  {
      $client  = @$_SERVER['HTTP_CLIENT_IP'];
      $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
      $remote  = $_SERVER['REMOTE_ADDR'];

      if(filter_var($client, FILTER_VALIDATE_IP)) {
          $ip = $client;
      }
      elseif(filter_var($forward, FILTER_VALIDATE_IP)) {
          $ip = $forward;
      }
      else {
          $ip = $remote;
      }

      return $ip;
  }

 /**
   * Define constant for host.
   */
  define("CLIENT", $_SERVER['HTTP_HOST']);

  /**
   * Define constant for view path.
   */
  define("VIEW_BASENAME", basename($_SERVER['REQUEST_URI']));
  
  /**
   * Define constant for user IP.
   */
  define('USER_IP', getUserIP());

  /**
   * Define constant for the users' user agent.
   */
  define('USER_AGENT', $_SERVER['HTTP_USER_AGENT']);

  /**
   * Define constant for the user login time.
   */
  define('LOGIN_TIME', $getTime);

  /**
   * Constant for the current version number.
   */
  define("FRAME_VERSION", "1.0", true);

  /**
   * Define classes dir path constant.
   */ 
  define("SETTINGS_DIR", "settings/", true);

  /**
   * Define classes dir path constant.
   */ 
  define("CLASSES_DIR", "classes/", true);

  /**
   * Define view controllers dir path constant.
   */ 
  define("VIEW_CONTROLLERS_DIR", "controllers/views/", true);

  /**
   * Define controllers dir path constant.
   */ 
  define("CONTROLLERS_DIR", "controllers/", true);

  /**
   * Define variables dir path constant.
   */ 
  define("VARS_DIR", "vars/", true);

  /**
   * Define partials dir path constant.
   */ 
  define("PARTIALS_DIR", "partials/", true);

  /**
   * Define views dir path constant.
   */ 
  define("VIEWS_DIR", "views/", true);

  /**
   * Define error views dir path constant.
   */ 
  define("ERROR_DIR", "error_msgs/", true);

  /**
   * Define assets dir path constant.
   */ 
  define("ASSETS_DIR", "assets/", true);

  /**
   * Define data dir path constant.
   */ 
  define("DATA_DIR", "data/", true);

  /**
   * Define data dir path constant.
   */ 
  define("COMPONENTS_DIR", "components/", true);
  
?>