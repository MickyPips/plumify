<?php
  /**
   * Main global controller file.
   */ 

  if(isset($_SESSION) && !empty($_SESSION)):

    /**
     * Get 'user' session.
     * @var session
     */
    $user = $_SESSION['user'];

    /**
     * Get the users' id.
     * @var session paramater 
     */
    $uName = $user['name'];

  endif;

  /**
   * Require all classes.
   */
  $files = glob(__DIR__ . '/../classes/*.php');

  foreach ($files as $file) {
      require($file);   
  }

  /**
   * Set default timezone. 
   */
  date_default_timezone_set("Europe/London"); 

  /**
   * Function to redirect user.
   * @param string $path Path of the page to redirect to. Don't include the first forward slash
   */
  function redirect($path)
  {
    header("Location: " . (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER["HTTP_HOST"] . "/" . $path . "");
  }

  /**
   * Function to check current URI and redirect to sign in view.
   * @param string $signInURL URL of the sign in view.
   */
  function reDirectToLogin($signInURL)
  {
    if(basename($_SERVER['REQUEST_URI']) != $signInURL) {
      redirect($signInURL);
    }  
  } 

  /**
   * Function to display bootstraps modal component.
   *
   * Pass arguments to the function via an array to override the default paramaters - title, body, triggerBtnTxt, dismissBtnTxt, size, transition
   * and showDismissBtn. 
   *
   * @link https://v4-alpha.getbootstrap.com/components/modal/ More information about Bootstraps modal component
   *
   * @param array $arrayParam Array can have title, body, triggerBtnTxt, dismissBtnTxt, size, transition and showDismissBtn values
   *
   * @return html Returns the trigger button and bootstrap modal markup
   */
  function ajaxRes($arrayParam = array())
  {

    $title = isset($arrayParam['title']) ? $arrayParam['title'] : "Default title";
    $body = isset($arrayParam['body']) ? $arrayParam['body'] : "Default body";
    $triggerBtnTxt = isset($arrayParam['triggerBtnTxt']) ? $arrayParam['triggerBtnTxt'] : "Open";
    $dismissBtnTxt = isset($arrayParam['dismissBtnTxt']) ? $arrayParam['dismissBtnTxt'] : "OK";
    $size = isset($arrayParam['size']) ? $arrayParam['size'] : "";
    $transition = isset($arrayParam['transition']) ? $arrayParam['transition'] : "fade";
    $showDismissBtn = isset($arrayParam['showDismissBtn']) ? $arrayParam['showDismissBtn'] : false;
    $autoShow = isset($arrayParam['autoShow']) ? $arrayParam['autoShow'] : false;
    $hideTrigger = isset($arrayParam['hideTrigger']) ? $arrayParam['hideTrigger'] : false;

    $showDismissBtn ? $disBtn = "<button type='button' class='btn btn-default' data-dismiss='modal'>$dismissBtnTxt</button>" : $disBtn = "";

    $autoShow || $hideTrigger ? $modal = "" : $modal = "<button type='button' class='btn btn-info btn-lg' data-toggle='modal' data-target='#ajaxRes'>$triggerBtnTxt</button>";

    $modal .= <<<OUT
      <div id="ajaxRes" class="modal $size $transition" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center">$title</h4>
            </div>
            <div class="spinner-cont"><span></span></div>
            <div class="modal-body">
              <p>$body</p>
            </div>
            <div class="modal-footer">
              $disBtn
            </div>
          </div>
        </div>
      </div>      
OUT;
    
    if($autoShow) {

      $modal .= <<<OUT
        <script type="text/javascript">
          $(document).ready(function () {
            $('#ajaxRes').modal('show');
          });
        </script>
OUT;
    }

    return $modal;
    
  }  

  /**
   * Recursive in_array function
   * @param string $needle
   * @param array $haystack
   * @param boolean $strict Default is false
   */
  function in_array_r($needle, $haystack, $strict = false) {
      foreach ($haystack as $item) {
          if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
              return true;
          }
      }
      return false;
  }

  /**
   * Get grammatically correct pronoun for user
   * @param string $gender
   * @return string
   */
  function getPronoun($gender) {

    $pronoun = "";

    $gender === "male" ? $pronoun = "his" : $pronoun = "her";

    return $pronoun;

  }

  /**
   * Set the global title of the site.
   * @var string
   */
  $setSiteTitle = \Site\Info::setSiteTitle("Plumify");


  if(isset($_SESSION['user'])) {

    /**
     * Get logged in users details.
     */
    function getSingleUser($email, $data) {

      $i = 1;

      foreach ($data as $user) {

        if($user['email'] === $email) {

          unset($user['password']);

          return $user;

          break;

        } else {

          if($i === count($data) && $userDetails === false) {

            $res = array("res" => "error", "msg" => "Could not find user.");
            echo json_encode($res);
          }
        }
        $i++;
      }
    }

    /**
     * Set the global user data for the site.
     */
    $data = new Data('user');

    $allUsers = $data->getData();

    $currentUser = getSingleUser($_SESSION['user']['email'], $allUsers);
    
    $latestUpdate = $data->latestUpdate();

    /**
     * Set the track data for the site.
     */
    $trackData = new Data('track');

    $allTracks = $trackData->getData();
    
  }

?>