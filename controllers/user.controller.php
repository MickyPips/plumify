<?php
  /**
   * Controller for user interactions.
   */

  /**
   * Require the main global controller file.
   */
  include_once(__DIR__ . '/../config.php');

  /**
   * Require Data class
   */
  include_once(__DIR__ . '/../classes/Data.php');

  /**
   * User login function.
   * @param object $shConn PDO connection object for shared DB
   * @param object $clConn PDO connection object for client DB
   * @param string $email Email address for user
   * @param string $password Password for user
   */
  function userLogin($data, $email, $password)
  {

    $userDetails = false;
    
    $newData = new Data($data);

    $userData = (array) $newData->getData();

    $i = 1;

    foreach ($userData as $user) {

      if($user['email'] === $email && $user['password'] === $password) {

        $userDetails = true;
        
        if(defined('USER_IP') && defined('USER_AGENT') && defined('LOGIN_TIME') && $userDetails) {

          $_SESSION['auth'] = md5( USER_AGENT . USER_IP . LOGIN_TIME);

          $_SESSION['user'] = array ('name' => $user['name'], 'email' => $user['email']);          

          $res = array("res" => "success");
          echo json_encode($res);

        } else {
          $res = array("res" => "error", "msg" => "Cannot start session.");
          echo json_encode($res);
        }

        break;

      } else {

        if($i === count($userData) && !$userDetails) {
          $res = array("res" => "error", "msg" => "Incorrect username or password. Please try again.");
          echo json_encode($res);
        }
      }
      $i++;
    }
  }  

  /**
   * Update a user.
   * @param object $conn
   * @param array $data
   * @return json|headers
   */
  function updateUser($conn, $data) {

    isset($_SESSION) ? '' : session_start();

    date_default_timezone_set("Europe/London");
    $date = date("Y-m-d H:i:s");

    try {

      $checkEmail = callSP($conn, "check_usrEmail", array($data['id'], $data['email']));

      if(empty($checkEmail)) {

        try {

          $updateUser = callSP($conn, "update_usr", array($data['id'], $data['title'], $data['first_name'], $data['last_name'], $data['email'], $data['phone'], $data['alt_phone'], $data['role'], $_SESSION['user']['uID'], $date), "update");

          $res = array("res" => "success", "msg" => "User " . $data["first_name"] . " " . $data["last_name"] . " has been updated.");

          echo json_encode($res);
          
          return true;

        } catch(Exception $e) {

          $res = array("res" => "error", "msg" => "Could not update " . $data['first_name'] . " " . $data['last_name'] . " - " . $e->getMessage());
          echo json_encode($res);
          return false;
        }

      } else {

        $id = $data['id'];
        $email = $data['email'];

        $res = array("res" => "error", "msg" => " user with that email address already exists.");
        echo json_encode($res);
        return false;
      }


    } catch(Exception $e) { 
      echo $e->getMessage();
    }

  }

?>