//fgnass.github.com/spin.js#v1.2.7
!function(e,t,n){function o(e,n){var r=t.createElement(e||"div"),i;for(i in n)r[i]=n[i];return r}function u(e){for(var t=1,n=arguments.length;t<n;t++)e.appendChild(arguments[t]);return e}function f(e,t,n,r){var o=["opacity",t,~~(e*100),n,r].join("-"),u=.01+n/r*100,f=Math.max(1-(1-e)/t*(100-u),e),l=s.substring(0,s.indexOf("Animation")).toLowerCase(),c=l&&"-"+l+"-"||"";return i[o]||(a.insertRule("@"+c+"keyframes "+o+"{"+"0%{opacity:"+f+"}"+u+"%{opacity:"+e+"}"+(u+.01)+"%{opacity:1}"+(u+t)%100+"%{opacity:"+e+"}"+"100%{opacity:"+f+"}"+"}",a.cssRules.length),i[o]=1),o}function l(e,t){var i=e.style,s,o;if(i[t]!==n)return t;t=t.charAt(0).toUpperCase()+t.slice(1);for(o=0;o<r.length;o++){s=r[o]+t;if(i[s]!==n)return s}}function c(e,t){for(var n in t)e.style[l(e,n)||n]=t[n];return e}function h(e){for(var t=1;t<arguments.length;t++){var r=arguments[t];for(var i in r)e[i]===n&&(e[i]=r[i])}return e}function p(e){var t={x:e.offsetLeft,y:e.offsetTop};while(e=e.offsetParent)t.x+=e.offsetLeft,t.y+=e.offsetTop;return t}var r=["webkit","Moz","ms","O"],i={},s,a=function(){var e=o("style",{type:"text/css"});return u(t.getElementsByTagName("head")[0],e),e.sheet||e.styleSheet}(),d={lines:12,length:7,width:5,radius:10,rotate:0,corners:1,color:"#000",speed:1,trail:100,opacity:.25,fps:20,zIndex:2e9,className:"spinner",top:"auto",left:"auto",position:"relative"},v=function m(e){if(!this.spin)return new m(e);this.opts=h(e||{},m.defaults,d)};v.defaults={},h(v.prototype,{spin:function(e){this.stop();var t=this,n=t.opts,r=t.el=c(o(0,{className:n.className}),{position:n.position,width:0,zIndex:n.zIndex}),i=n.radius+n.length+n.width,u,a;e&&(e.insertBefore(r,e.firstChild||null),a=p(e),u=p(r),c(r,{left:(n.left=="auto"?a.x-u.x+(e.offsetWidth>>1):parseInt(n.left,10)+i)+"px",top:(n.top=="auto"?a.y-u.y+(e.offsetHeight>>1):parseInt(n.top,10)+i)+"px"})),r.setAttribute("aria-role","progressbar"),t.lines(r,t.opts);if(!s){var f=0,l=n.fps,h=l/n.speed,d=(1-n.opacity)/(h*n.trail/100),v=h/n.lines;(function m(){f++;for(var e=n.lines;e;e--){var i=Math.max(1-(f+e*v)%h*d,n.opacity);t.opacity(r,n.lines-e,i,n)}t.timeout=t.el&&setTimeout(m,~~(1e3/l))})()}return t},stop:function(){var e=this.el;return e&&(clearTimeout(this.timeout),e.parentNode&&e.parentNode.removeChild(e),this.el=n),this},lines:function(e,t){function i(e,r){return c(o(),{position:"absolute",width:t.length+t.width+"px",height:t.width+"px",background:e,boxShadow:r,transformOrigin:"left",transform:"rotate("+~~(360/t.lines*n+t.rotate)+"deg) translate("+t.radius+"px"+",0)",borderRadius:(t.corners*t.width>>1)+"px"})}var n=0,r;for(;n<t.lines;n++)r=c(o(),{position:"absolute",top:1+~(t.width/2)+"px",transform:t.hwaccel?"translate3d(0,0,0)":"",opacity:t.opacity,animation:s&&f(t.opacity,t.trail,n,t.lines)+" "+1/t.speed+"s linear infinite"}),t.shadow&&u(r,c(i("#000","0 0 4px #000"),{top:"2px"})),u(e,u(r,i(t.color,"0 0 1px rgba(0,0,0,.1)")));return e},opacity:function(e,t,n){t<e.childNodes.length&&(e.childNodes[t].style.opacity=n)}}),function(){function e(e,t){return o("<"+e+' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">',t)}var t=c(o("group"),{behavior:"url(#default#VML)"});!l(t,"transform")&&t.adj?(a.addRule(".spin-vml","behavior:url(#default#VML)"),v.prototype.lines=function(t,n){function s(){return c(e("group",{coordsize:i+" "+i,coordorigin:-r+" "+ -r}),{width:i,height:i})}function l(t,i,o){u(a,u(c(s(),{rotation:360/n.lines*t+"deg",left:~~i}),u(c(e("roundrect",{arcsize:n.corners}),{width:r,height:n.width,left:n.radius,top:-n.width>>1,filter:o}),e("fill",{color:n.color,opacity:n.opacity}),e("stroke",{opacity:0}))))}var r=n.length+n.width,i=2*r,o=-(n.width+n.length)*2+"px",a=c(s(),{position:"absolute",top:o,left:o}),f;if(n.shadow)for(f=1;f<=n.lines;f++)l(f,-2,"progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)");for(f=1;f<=n.lines;f++)l(f);return u(t,a)},v.prototype.opacity=function(e,t,n,r){var i=e.firstChild;r=r.shadow&&r.lines||0,i&&t+r<i.childNodes.length&&(i=i.childNodes[t+r],i=i&&i.firstChild,i=i&&i.firstChild,i&&(i.opacity=n))}):s=l(t,"animation")}(),typeof define=="function"&&define.amd?define(function(){return v}):e.Spinner=v}(window,document);
/*

You can now create a spinner using any of the variants below:

$("#el").spin(); // Produces default Spinner using the text color of #el.
$("#el").spin("small"); // Produces a 'small' Spinner using the text color of #el.
$("#el").spin("large", "white"); // Produces a 'large' Spinner in white (or any valid CSS color).
$("#el").spin({ ... }); // Produces a Spinner using your custom settings.

$("#el").spin(false); // Kills the spinner.

*/
(function($) {
	$.fn.spin = function(opts, color) {
		var presets = {
			"ajax_loading_small": {
				lines: 12, // The number of lines to draw
				length: 4, // The length of each line
				width: 3, // The line thickness
				radius: 8, // The radius of the inner circle
				corners: 1, // Corner roundness (0..1)
				rotate: 0, // The rotation offset
				color: 'rgba(85,85,85,0.9)', // #rgb or #rrggbb
				speed: 1, // Rounds per second
				trail: 60, // Afterglow percentage
				shadow: false, // Whether to render a shadow
			 	hwaccel: true, // Whether to use hardware acceleration
				className: 'spinner', // The CSS class to assign to the spinner
				zIndex: 2e9, // The z-index (defaults to 2000000000)
				top: 0, // Top position relative to parent in px
				left: 0 // Left position relative to parent in px
			},
		};
		if (Spinner) {
			return this.each(function() {
				var $this = $(this),
					data = $this.data();
				
				if (data.spinner) {
					data.spinner.stop();
					delete data.spinner;
				}
				if (opts !== false) {
					if (typeof opts === "string") {
						if (opts in presets) {
							opts = presets[opts];
						} else {
							opts = {};
						}
						if (color) {
							opts.color = color;
						}
					}
					data.spinner = new Spinner($.extend({color: $this.css('color')}, opts)).spin(this);
				}
			});
		} else {
			throw "Spinner class not available.";
		}
	};
})(jQuery);
/**
 * Update function for date and time display.
 */
var timeOutDelay = 1000;

/**
 * Show spinner.
 */
function startSpinner(spinner = true) {

	if(spinner === true) {
		$('#ajaxRes .spinner-cont span').spin('ajax_loading_small');
		$('#ajaxRes').addClass('loading-data');
		$('#ajaxRes .modal-dialog').addClass('modal-sm');
		$('#ajaxRes button').hide();
		$('#ajaxRes .modal-title').text('');
		$('#ajaxRes .modal-body').text('');
	}

	$('#ajaxRes').modal('show');

}

/**
 * Hide spinner.
 */
function stopSpinner(spinner = true) {

	if(spinner === true) {

		setTimeout(function() {		

			$('#ajaxRes').removeClass('loading-data');
			$('#ajaxRes .modal-dialog').removeClass('modal-sm');
			$('#ajaxRes .spinner-cont span').spin(false);

		}, timeOutDelay);
	}

}

/**
 * Modify modal title and body text.
 * @param string title Title for the modal
 * @param string body Body text for the modal
 */
function modalTxt(args, spinner = true) {

	$('#ajaxRes button.confirm, #ajaxRes button.cancel').remove();
	
	var populateModal = function(args) {

		$('#ajaxRes button').show();

		$('#ajaxRes .modal-title').text(args.title);
		$('#ajaxRes .modal-body').text(args.body);

		$('#ajaxRes .modal-footer').append('<button type="button" class="btn btn-success confirm" data-dismiss="modal">OK</button>');

		if(args.confirmation === true) {
			$('#ajaxRes .modal-footer').append('<button type="button" class="btn btn-danger cancel" data-dismiss="modal">Cancel</button>');
		}

		if(args.hasOwnProperty('options')) {

			if(args.options.hasOwnProperty('successRes') || args.options.hasOwnProperty('cancelRes')) {
				$('#ajaxRes .confirm').off('click').on('click', function() {	
					args.options.confAnswer === 'cancel' ? args.options.cancelRes(args.cancelParam) : args.options.successRes(args.successParam);
				});		
			}
		}
	}

	if(spinner === true) {
		
		setTimeout(function() {		
			populateModal(args);

			$('#ajaxRes').is(':visible') ? '' : $('#ajaxRes').modal('show');

		}, timeOutDelay);

	} else {
		populateModal(args);
		$('#ajaxRes').modal('show');
	}	
}

/**
 * Checks to see if the request needs to be confirmed before passing it onto the AJAX function.
 * @param object req Object containing all the data for the action
 * @param boolean spinner Optional, default true
 */
function confirmationCheck(req, spinner = true) {

	if(req.confirmation === true) {

		stopSpinner(spinner);

		var args = {

					'title': 'Confirmation',
					'body': req.confirmationMsg,
					'confirmation': req.confirmation,
					'options': req
				}

		modalTxt(args, false);
		$('#ajaxRes .confirm').removeAttr('data-dismiss');
		$('#ajaxRes .confirm').off('click').on('click', function() {
			ajaxCall(req, true);
		});

		if(req.cancelRes) {
			$('#ajaxRes .cancel').removeAttr('data-dismiss');
			$('#ajaxRes .cancel').off('click').on('click', function() {
				req.confAnswer = 'cancel';
				ajaxCall(req, true);
			});
		}
	
	} else {
		ajaxCall(req, spinner);
	}
}

/**
 * Ajax function for data manipulation.
 * @param array req
 */
function ajaxCall(req, spinner = true) {

	startSpinner(spinner);

	$.ajax({
		url: req.url,
		type: req.reqType,
		data: req.postData,
		error:function (xhr, ajaxOptions, thrownError){

			stopSpinner(spinner);

			modalTxt({'title': 'Error', 'body': thrownError});

			if(req.errorRes) {
				req.errorRes();
			}

		},
		success: function(res) {

			stopSpinner(spinner);

			try {
			    var resMsg = $.parseJSON(res);
			}
			catch(err) {				
			    modalTxt({'title': 'Error', 'body': err + '\r\n' + res});
			}

			if(resMsg) {

				if(resMsg.res === 'error') {

					modalTxt({'title': 'Error', 'body': resMsg.msg});
					
				} else {
					
					var responseParamater = resMsg.successParam ? resMsg.successParam : '',
							cancelParamater = resMsg.cancelParam ? resMsg.cencelParam : '';

					if(req.successModal === true) {

						modalTxt({'title': 'Success', 'body': resMsg.msg, 'options': req, 'successParam': responseParamater});
					
					} else {

						if(req.successRes && !req.confirmation) {

							req.successRes(responseParamater);
						
						}

						if(req.cancelRes && !req.confirmation) {

							req.cancelRes(cancelParamater);

						}
					}
				}
			}
		}
	});
}

function showHideSidebar(ele) {
	
	ele.stop().toggleClass('sbopen');

	$('#aside.sidebar').toggle('slide', 'left', 500);

	ele.find('.fa').toggle();

	$('#page-content').stop().toggleClass('col-md-10 col-md-offset-2');
}

/**
 * Overwrite default validation messages
 */
$.extend(jQuery.validator.messages, {
    required: "This field is required.",
    remote: "Please fix this field.",
    email: "Invlaid email.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Invlaid number",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
});

function trackNotification(theBody, theIcon, theTitle) {
  var options = {
      body: theBody,
      icon: theIcon
  }
  var n = new Notification(theTitle,options);
}

/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 * Taken from https://codepen.io/gapcode/pen/vEJNZN
 */
function detectIE() {
  var ua = window.navigator.userAgent,
  		msie = ua.indexOf('MSIE '),
  		trident = ua.indexOf('Trident/');
  
  if (msie > 0 || trident > 0) {
    // IE 11 or IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  // other browser
  return false;
}

$(document).ready(function() {

	/**
	 * Fix sticky footer 'flex' IE issue
	 */
	if(detectIE) {
		$('body').css('height', '100vh');
	}

	if(location.pathname != '/sign-in') {

		Notification.requestPermission();
	
	}

});
function userLogin() {

	$('form#loginForm').validate({

		rules: {
			email: {
				required: true,
				email: true
			},
			password: {
				required: true
			}
		},
		submitHandler: function() {

			var data = {
				'reqType': 'POST',
				'url': '/api/users-api.php',
				'postData': {
					'req': 'usr_login',
					'dataset': $('#loginForm #user-signin').attr('data-set'),
					'email': $('#loginForm #email').val(),
			    'password': $('#loginForm #password').val()
			   },
			    'errorRes': function() { $('#loginForm input').val(null); },
			    'successModal': false,
			    'successRes': function() { window.location.href = '/'; }
		  	};

			confirmationCheck(data);
		}

	});

};

function userLogout() {
	document.location = '/logout.php';
}
function resetPlayer(player) {

	$('.active').removeClass('active');
	$('.pause').toggleClass('pause play');
	$('#player').attr('src', '');
	$('#player-controls .track-title').text('').html('Choose a song...');
	$('.current-thumb').attr('class', 'current-thumb');

}

function playTrack(player, ele) {

	resetPlayer(player);

	ele.addClass('active');

	$('button.play').addClass('active').toggleClass('play pause');

	$('button.shuffle').removeClass('active');

	player.get(0).pause();
	player.get(0).currentTime = 0;

	trackNotification(ele.attr('data-desc'), '/assets/imgs/thumbs/' + ele.attr('data-thumb') + '@2x.png', ele.attr('data-title'));
	player.attr('src', '/assets/music/' + ele.attr('data-track'));
	
	player.get(0).play();

	updateTrackInfo(player);

	player.one('ended', function() {

		resetPlayer(player);
		$('.album-list .active').removeClass('active');

	});

}

function pauseTrack(player) {

	player.get(0).paused ? '' : player.get(0).pause();

}

/**
 * Randomize array element order in-place.
 * Using Durstenfeld shuffle algorithm.
 * Taken from https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
*/
function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

function shuffleTracks(player, ele) {

	var allTracks = [];

	function playRandom(tracks) {

		$('.album-list .active').removeClass('active');

		var track = tracks[Math.floor(Math.random()*tracks.length)],
				activeTrack = $('#' + track.split('.')[0]),
				trackThumb = activeTrack.attr('data-thumb'),
				trackTitle = activeTrack.attr('data-title'),
				trackDesc = activeTrack.attr('data-desc');

		activeTrack.addClass('active');

		trackNotification(trackDesc, '/assets/imgs/thumbs/' + trackThumb + '@2x.png', trackTitle);

		player.attr('src', '/assets/music/' + track);

		$('button.play').addClass('active');

		player.get(0).play();

		updateTrackInfo(player);
	}

	$('.album-list [class^="album-"]').each(function() {
		allTracks.push($(this).attr('data-track'));
	});


	if(!ele.is('.active')) {

		resetPlayer(player);
		playRandom(allTracks);
		$('button.play').addClass('active');

	}

	ele.toggleClass('active');
	
	player.on('ended', function() {

		ele.is('.active') ? playRandom(allTracks) : resetPlayer(player);

	});

}

function loopTrack(player, ele) {

	if(!player.get(0).paused) {

		$('button.play').addClass('active');

		ele.toggleClass('active');

		player.prop('loop') ? player.prop('loop', false) : player.prop('loop', true);
	}

}

/**
 * Taken from https://stackoverflow.com/questions/6500526/convert-time-by-jquery
 */
function convertSeconds(seconds) {

	var time = parseInt(seconds, 10);
			time = time < 0 ? 0 : time;

	var minutes = Math.floor(time / 60);
	var seconds = time % 60;

	minutes = minutes < 10 ? "0"+minutes : minutes;
	seconds = seconds < 10 ? "0"+seconds : seconds;

	var formattedTime = minutes+":"+seconds;

	return formattedTime;

}

function updateTrackInfo(player) {

	if(!player.get(0).paused) {

		var activeTrack = $('.album-list .active');

		player.on('loadedmetadata', function() {

			var currentTime = parseInt(player.get(0).currentTime, 10),
					duration = parseInt(player.get(0).duration, 10),
					counter = convertSeconds(Math.round(duration - currentTime));

			$('#scrub').attr('max', duration);
			$('.track-time').text(counter);		

		});
		
		player.on('timeupdate', function() {

			if(!player.get(0).paused) {
				$('.track-time').text(convertSeconds(Math.round(parseInt(player.get(0).duration, 10) - parseInt(player.get(0).currentTime, 10))));
			} else {
				$('#player-controls .track-time').text('00:00');
			}

			$('#scrub').val(parseInt(player.get(0).currentTime, 10)); 

		});

		$('#scrub').on('change', function() {

			var currentTime = player.get(0).currentTime,
					duration = parseInt(player.get(0).duration, 10);

			player.get(0).currentTime = $(this).val();

			$('#scrub').attr('max', duration);

		});

		$('.track-title').text(activeTrack.attr('data-title'));

		$('.current-thumb').attr('class', 'current-thumb playing ' + activeTrack.attr('data-thumb'));

	}
}
$(document).ready(function() {

	var player = $('#player');

	$('#user-signin').off('click').on('click', function(e) {
		
		userLogin();

	});

	$('#main-menu .logout').off('click').on('click', function(e) {
		userLogout();
	});

	$('#profile-img').off('click').on('click', function(e) {

		$('#main-menu ul').stop().slideToggle();

	});

	$('.album-list [class^="album-"]').off('click').on('click', function() {

		playTrack($('#player'), $(this));

	});

	$('#player-controls button').off('click').on('click', function(e) {
			
			e.preventDefault();

			var player = $('#player');

			switch(true) {

				case ($(this).is('.play')):
				case ($(this).is('.pause')):

					if(player.attr('src') != '') {

						if(!player.get(0).paused) {

						 	player.get(0).pause();
							$(this).toggleClass('pause play');
						
						} else {

							player.get(0).play();
							$(this).toggleClass('play pause');

						}
					}

					break;
				case ($(this).is('.shuffle')):
					shuffleTracks(player, $(this));
					break;
				case ($(this).is('.repeat')):
					loopTrack(player, $(this));
					break;
			}
	});

});