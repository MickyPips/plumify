$(document).ready(function() {

	var player = $('#player');

	$('#user-signin').off('click').on('click', function(e) {
		
		userLogin();

	});

	$('#main-menu .logout').off('click').on('click', function(e) {
		userLogout();
	});

	$('#profile-img').off('click').on('click', function(e) {

		$('#main-menu ul').stop().slideToggle();

	});

	$('.album-list [class^="album-"]').off('click').on('click', function() {

		playTrack($('#player'), $(this));

	});

	$('#player-controls button').off('click').on('click', function(e) {
			
			e.preventDefault();

			var player = $('#player');

			switch(true) {

				case ($(this).is('.play')):
				case ($(this).is('.pause')):

					if(player.attr('src') != '') {

						if(!player.get(0).paused) {

						 	player.get(0).pause();
							$(this).toggleClass('pause play');
						
						} else {

							player.get(0).play();
							$(this).toggleClass('play pause');

						}
					}

					break;
				case ($(this).is('.shuffle')):
					shuffleTracks(player, $(this));
					break;
				case ($(this).is('.repeat')):
					loopTrack(player, $(this));
					break;
			}
	});

});