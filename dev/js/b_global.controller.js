/**
 * Update function for date and time display.
 */
var timeOutDelay = 1000;

/**
 * Show spinner.
 */
function startSpinner(spinner = true) {

	if(spinner === true) {
		$('#ajaxRes .spinner-cont span').spin('ajax_loading_small');
		$('#ajaxRes').addClass('loading-data');
		$('#ajaxRes .modal-dialog').addClass('modal-sm');
		$('#ajaxRes button').hide();
		$('#ajaxRes .modal-title').text('');
		$('#ajaxRes .modal-body').text('');
	}

	$('#ajaxRes').modal('show');

}

/**
 * Hide spinner.
 */
function stopSpinner(spinner = true) {

	if(spinner === true) {

		setTimeout(function() {		

			$('#ajaxRes').removeClass('loading-data');
			$('#ajaxRes .modal-dialog').removeClass('modal-sm');
			$('#ajaxRes .spinner-cont span').spin(false);

		}, timeOutDelay);
	}

}

/**
 * Modify modal title and body text.
 * @param string title Title for the modal
 * @param string body Body text for the modal
 */
function modalTxt(args, spinner = true) {

	$('#ajaxRes button.confirm, #ajaxRes button.cancel').remove();
	
	var populateModal = function(args) {

		$('#ajaxRes button').show();

		$('#ajaxRes .modal-title').text(args.title);
		$('#ajaxRes .modal-body').text(args.body);

		$('#ajaxRes .modal-footer').append('<button type="button" class="btn btn-success confirm" data-dismiss="modal">OK</button>');

		if(args.confirmation === true) {
			$('#ajaxRes .modal-footer').append('<button type="button" class="btn btn-danger cancel" data-dismiss="modal">Cancel</button>');
		}

		if(args.hasOwnProperty('options')) {

			if(args.options.hasOwnProperty('successRes') || args.options.hasOwnProperty('cancelRes')) {
				$('#ajaxRes .confirm').off('click').on('click', function() {	
					args.options.confAnswer === 'cancel' ? args.options.cancelRes(args.cancelParam) : args.options.successRes(args.successParam);
				});		
			}
		}
	}

	if(spinner === true) {
		
		setTimeout(function() {		
			populateModal(args);

			$('#ajaxRes').is(':visible') ? '' : $('#ajaxRes').modal('show');

		}, timeOutDelay);

	} else {
		populateModal(args);
		$('#ajaxRes').modal('show');
	}	
}

/**
 * Checks to see if the request needs to be confirmed before passing it onto the AJAX function.
 * @param object req Object containing all the data for the action
 * @param boolean spinner Optional, default true
 */
function confirmationCheck(req, spinner = true) {

	if(req.confirmation === true) {

		stopSpinner(spinner);

		var args = {

					'title': 'Confirmation',
					'body': req.confirmationMsg,
					'confirmation': req.confirmation,
					'options': req
				}

		modalTxt(args, false);
		$('#ajaxRes .confirm').removeAttr('data-dismiss');
		$('#ajaxRes .confirm').off('click').on('click', function() {
			ajaxCall(req, true);
		});

		if(req.cancelRes) {
			$('#ajaxRes .cancel').removeAttr('data-dismiss');
			$('#ajaxRes .cancel').off('click').on('click', function() {
				req.confAnswer = 'cancel';
				ajaxCall(req, true);
			});
		}
	
	} else {
		ajaxCall(req, spinner);
	}
}

/**
 * Ajax function for data manipulation.
 * @param array req
 */
function ajaxCall(req, spinner = true) {

	startSpinner(spinner);

	$.ajax({
		url: req.url,
		type: req.reqType,
		data: req.postData,
		error:function (xhr, ajaxOptions, thrownError){

			stopSpinner(spinner);

			modalTxt({'title': 'Error', 'body': thrownError});

			if(req.errorRes) {
				req.errorRes();
			}

		},
		success: function(res) {

			stopSpinner(spinner);

			try {
			    var resMsg = $.parseJSON(res);
			}
			catch(err) {				
			    modalTxt({'title': 'Error', 'body': err + '\r\n' + res});
			}

			if(resMsg) {

				if(resMsg.res === 'error') {

					modalTxt({'title': 'Error', 'body': resMsg.msg});
					
				} else {
					
					var responseParamater = resMsg.successParam ? resMsg.successParam : '',
							cancelParamater = resMsg.cancelParam ? resMsg.cencelParam : '';

					if(req.successModal === true) {

						modalTxt({'title': 'Success', 'body': resMsg.msg, 'options': req, 'successParam': responseParamater});
					
					} else {

						if(req.successRes && !req.confirmation) {

							req.successRes(responseParamater);
						
						}

						if(req.cancelRes && !req.confirmation) {

							req.cancelRes(cancelParamater);

						}
					}
				}
			}
		}
	});
}

function showHideSidebar(ele) {
	
	ele.stop().toggleClass('sbopen');

	$('#aside.sidebar').toggle('slide', 'left', 500);

	ele.find('.fa').toggle();

	$('#page-content').stop().toggleClass('col-md-10 col-md-offset-2');
}

/**
 * Overwrite default validation messages
 */
$.extend(jQuery.validator.messages, {
    required: "This field is required.",
    remote: "Please fix this field.",
    email: "Invlaid email.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Invlaid number",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
});

function trackNotification(theBody, theIcon, theTitle) {
  var options = {
      body: theBody,
      icon: theIcon
  }
  var n = new Notification(theTitle,options);
}

/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 * Taken from https://codepen.io/gapcode/pen/vEJNZN
 */
function detectIE() {
  var ua = window.navigator.userAgent,
  		msie = ua.indexOf('MSIE '),
  		trident = ua.indexOf('Trident/');
  
  if (msie > 0 || trident > 0) {
    // IE 11 or IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  // other browser
  return false;
}

$(document).ready(function() {

	/**
	 * Fix sticky footer 'flex' IE issue
	 */
	if(detectIE) {
		$('body').css('height', '100vh');
	}

	if(location.pathname != '/sign-in') {

		Notification.requestPermission();
	
	}

});