function userLogin() {

	$('form#loginForm').validate({

		rules: {
			email: {
				required: true,
				email: true
			},
			password: {
				required: true
			}
		},
		submitHandler: function() {

			var data = {
				'reqType': 'POST',
				'url': '/api/users-api.php',
				'postData': {
					'req': 'usr_login',
					'dataset': $('#loginForm #user-signin').attr('data-set'),
					'email': $('#loginForm #email').val(),
			    'password': $('#loginForm #password').val()
			   },
			    'errorRes': function() { $('#loginForm input').val(null); },
			    'successModal': false,
			    'successRes': function() { window.location.href = '/'; }
		  	};

			confirmationCheck(data);
		}

	});

};

function userLogout() {
	document.location = '/logout.php';
}