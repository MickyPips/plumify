function resetPlayer(player) {

	$('.active').removeClass('active');
	$('.pause').toggleClass('pause play');
	$('#player').attr('src', '');
	$('#player-controls .track-title').text('').html('Choose a song...');
	$('.current-thumb').attr('class', 'current-thumb');

}

function playTrack(player, ele) {

	resetPlayer(player);

	ele.addClass('active');

	$('button.play').addClass('active').toggleClass('play pause');

	$('button.shuffle').removeClass('active');

	player.get(0).pause();
	player.get(0).currentTime = 0;

	trackNotification(ele.attr('data-desc'), '/assets/imgs/thumbs/' + ele.attr('data-thumb') + '@2x.png', ele.attr('data-title'));
	player.attr('src', '/assets/music/' + ele.attr('data-track'));
	
	player.get(0).play();

	updateTrackInfo(player);

	player.one('ended', function() {

		resetPlayer(player);
		$('.album-list .active').removeClass('active');

	});

}

function pauseTrack(player) {

	player.get(0).paused ? '' : player.get(0).pause();

}

/**
 * Randomize array element order in-place.
 * Using Durstenfeld shuffle algorithm.
 * Taken from https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
*/
function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

function shuffleTracks(player, ele) {

	var allTracks = [];

	function playRandom(tracks) {

		$('.album-list .active').removeClass('active');

		var track = tracks[Math.floor(Math.random()*tracks.length)],
				activeTrack = $('#' + track.split('.')[0]),
				trackThumb = activeTrack.attr('data-thumb'),
				trackTitle = activeTrack.attr('data-title'),
				trackDesc = activeTrack.attr('data-desc');

		activeTrack.addClass('active');

		trackNotification(trackDesc, '/assets/imgs/thumbs/' + trackThumb + '@2x.png', trackTitle);

		player.attr('src', '/assets/music/' + track);

		$('button.play').addClass('active');

		player.get(0).play();

		updateTrackInfo(player);
	}

	$('.album-list [class^="album-"]').each(function() {
		allTracks.push($(this).attr('data-track'));
	});


	if(!ele.is('.active')) {

		resetPlayer(player);
		playRandom(allTracks);
		$('button.play').addClass('active');

	}

	ele.toggleClass('active');
	
	player.on('ended', function() {

		ele.is('.active') ? playRandom(allTracks) : resetPlayer(player);

	});

}

function loopTrack(player, ele) {

	if(!player.get(0).paused) {

		$('button.play').addClass('active');

		ele.toggleClass('active');

		player.prop('loop') ? player.prop('loop', false) : player.prop('loop', true);
	}

}

/**
 * Taken from https://stackoverflow.com/questions/6500526/convert-time-by-jquery
 */
function convertSeconds(seconds) {

	var time = parseInt(seconds, 10);
			time = time < 0 ? 0 : time;

	var minutes = Math.floor(time / 60);
	var seconds = time % 60;

	minutes = minutes < 10 ? "0"+minutes : minutes;
	seconds = seconds < 10 ? "0"+seconds : seconds;

	var formattedTime = minutes+":"+seconds;

	return formattedTime;

}

function updateTrackInfo(player) {

	if(!player.get(0).paused) {

		var activeTrack = $('.album-list .active');

		player.on('loadedmetadata', function() {

			var currentTime = parseInt(player.get(0).currentTime, 10),
					duration = parseInt(player.get(0).duration, 10),
					counter = convertSeconds(Math.round(duration - currentTime));

			$('#scrub').attr('max', duration);
			$('.track-time').text(counter);		

		});
		
		player.on('timeupdate', function() {

			if(!player.get(0).paused) {
				$('.track-time').text(convertSeconds(Math.round(parseInt(player.get(0).duration, 10) - parseInt(player.get(0).currentTime, 10))));
			} else {
				$('#player-controls .track-time').text('00:00');
			}

			$('#scrub').val(parseInt(player.get(0).currentTime, 10)); 

		});

		$('#scrub').on('change', function() {

			var currentTime = player.get(0).currentTime,
					duration = parseInt(player.get(0).duration, 10);

			player.get(0).currentTime = $(this).val();

			$('#scrub').attr('max', duration);

		});

		$('.track-title').text(activeTrack.attr('data-title'));

		$('.current-thumb').attr('class', 'current-thumb playing ' + activeTrack.attr('data-thumb'));

	}
}