<aside id="user-info" role="menu" class="col-md-6 pull-right text-right">
  <div id="profile-img" class="<?php echo $currentUser['avatar'] ?> pull-right" title="Account information for <?php echo $currentUser['name'] ?>"></div>
  <nav id="main-menu" class="pull-right" role="navigation">
    <ul>
      <li><a class="help" href="/help-support" title="Help and support">Help and Support</a></li>
      <li><a class="preferences" href="/preferences" title="Preferences">Preferences</a></li>
      <li><a class="logout" href="#" title="Logout">Logout</a></li>
    </ul>
  </nav>
</aside>