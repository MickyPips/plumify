<section id="recent-share" class="col-sm-3 col-sm-offset-1">

	<?php

		$avatar = $latestUpdate['avatar'];
		$name = $latestUpdate['name'];
		$pronoun = getPronoun($latestUpdate['gender']);
		$share = $latestUpdate['recent_activity']['share']['title'];
		$type = $latestUpdate['recent_activity']['share']['type'];

		$activity = <<<OUT

			<span class="$avatar" title="$name"></span>
			<div class="activity-info">
				<p><b>$name</b></p>
				<p>Shared $pronoun <span class="green"><b><a href="#" title="Listen to $share $type">$share</a></b></span> $type</p>
			</div>
OUT;

		echo $activity;

	?>

</section>