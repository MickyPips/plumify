<aside id="player-bar" class="row">

	<section id="player-cont" class="col-sm-8">
		<audio id="player" src="">
		  Your browser does not support the audio element.
		</audio>

		<span class="col-xs-1">
			<span class="current-thumb"></span>
		</span>

		<div id="player-controls" class="col-xs-8 col-xs-offset-1">
			<div class="row">
				<h5 class="track-title">Choose a song...</h5>
			</div>
			<div class="row">
				<input type="range" id="scrub" value="0" max="" />
		  </div>
		  <div class="row control-cont">
			  <button class="play" title="Play track">Play</button> 
			  <button class="shuffle" title="Shuffle tracks">Shuffle</button> 
			  <button class="repeat" title="Repeat track">Repeat</button>
			  <span class="track-time pull-right">00:00</span>
		  </div> 
		</div>
	</section>

	<?php require_once("latest-update.php"); ?>

</aside>