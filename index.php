<?php
  
  /**
   * Index file for routing all http requests.
   *
   * Require config.php file that defines all constants, header.inc file that handles the file dependancy
   * of the currently active view and the footer.inc file that calls any other scripts that are needed.
   */
  require_once('config.php');

  require_once(PARTIALS_DIR . 'header.inc');

  require_once(PARTIALS_DIR . 'footer.inc');

?>