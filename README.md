﻿# Victoria Plumb Technical Test - Plumify - Michael Pierre

# plumify - VictoriaPlum.com Frontend Test.

## Brief
Recreate the provided Mockup into a responsive page. Discover the individual
building blocks that can be built independently and put them to use in order to
build the final user interface, using the assets provided.

## Requirements
* Must be fully responsive.
* Use of CSS processors (SCSS or PostCSS)
* Clean, semantic code

## Bonus points
* CSS Animations
* Assign an audio track to each album, When clicked the track should start playing,
  updating the UI accordingly.
* Use the Javascript Notification API to show notification of track changes
  (include track details and the album art)
* Make the player controls work.

##ToDo
Refactor jQuery for player to be OO and clean up code

Fix IE notification issue

Cross Browser compatibility, mainly IE
## Technology Stack
* PHP 7.1.4
* HTML(5)
* SASS / CSS(3)
* Bootstrap - Latest version pulled from CDN
* jQuery - Latest version pulled from CDN
* Ajax
* Babel
* Node.js / npm
* Grunt

## Setup
### Node.js / npm
Used to install Node.js and npm; if Node and npm already installed you can skip this step.
You can find documentation on how to install Node.js and npm for your system [here](https://docs.npmjs.com/getting-started/installing-node "Installing Node.js and updating npm")
### Task runner install
Installs node modules - Solely used for compiling js and scss in dev.
As well as installing the modules for the preprocessing this will also install Grunt and the Babel lib.
```shell
npm install
```
To do a fresh compile of js, css and modernizr run:
```shell
grunt
```
## User logins
User login details can be found in
```shell
/data/user.data.json
```
## Command list
### Grunt
```shell
grunt default
```
```shell
grunt watch
```
```shell
grunt compileSprites
```
```shell
grunt compileSCSS
```
```shell
grunt compileJS
```