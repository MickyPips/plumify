<?php
  /**
   * Global variables file.
   *
   * Human readable/memorable versions of class methods and properties.
   */  
  
  /**
   * Set the title for the site.
   * @var string Get the current global site title
   */
  $siteTitle = \Site\Info::getSiteTitle();

  /**
   * Get the current view.
   * @var string Human readable name of the current view
   */
  $currentView = \Site\Content::getCurrentView();

  /**
   * Get the controller for the current view.
   * @var string|boolean Returns file path of controller for current view if available, of not returns false
   */
  $currentController = \Site\Content::getController();

  /**
   * Get current view content.
   * @var string File path of the current view
   */
  $viewContent = \Site\Content::currentViewFile();

  /**
   * Check if there is a view for the requested page.
   * @var boolean
   */
  $viewExists = file_exists($viewContent);
  
  /**
   * Get the current view title.
   * @var boolean|string True or 'Error'
   */
  $viewTitle = $viewExists ? \Site\Content::viewTitle($currentView) : 'Error';

  /**
   * Get the meta title for the current view.
   * @var string 
   */
  $metaTitle = $siteTitle . " | " . $viewTitle;

  /**
   * Check if the current view is the home page.
   * @var boolean
   */
  $isHome = \Site\Content::isHome();

   /**
   * Check if the current view is the home page.
   * @var boolean
   */
  $isSignIn = \Site\Content::isSignIn();

  /**
   * Set the pathname for the sign in view.
   * @var string 
   */
  $signInURL = "sign-in";

?>